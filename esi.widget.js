$(esi_update_visibility);
$(function(){ $('#edit-esi-mode-0-wrapper').parent().change(function (){esi_update_visibility();})});
function esi_update_visibility() {
  var block_age_fieldset = $('#edit-esi-block-default-max-age').parents('fieldset');
  var panel_age_fieldset = $('#edit-esi-panel-default-max-age').parents('fieldset');
  // Disabled.
  if ($('#edit-esi-mode-0:checked').val() !== undefined) {
    $('#edit-esi-ajax-fallback-wrapper').hide();
    esi_update_cdn_ajax_visibility();
    $('#edit-esi-page-cache-wrapper').hide();
    $('#edit-esi-seed-rotation-interval-wrapper').hide();
    $('#edit-esi-role-user-direct-injection-wrapper').hide();
    $(block_age_fieldset[0]).hide();
    $(panel_age_fieldset[0]).hide();
    esi_update_cache_visibility();
  }
  // ESI.
  else if ($('#edit-esi-mode-1:checked').val() !== undefined) {
    $('#edit-esi-ajax-fallback-wrapper').show();
    esi_update_cdn_ajax_visibility();
    $('#edit-esi-page-cache-wrapper').show();
    $('#edit-esi-seed-rotation-interval-wrapper').show();
    $('#edit-esi-role-user-direct-injection-wrapper').show();
    $(block_age_fieldset[0]).show();
    $(panel_age_fieldset[0]).show();
    esi_update_cache_visibility();
  }
  // SSI.
  else if ($('#edit-esi-mode-2:checked').val() !== undefined) {
    $('#edit-esi-ajax-fallback-wrapper').hide();
    esi_update_cdn_ajax_visibility();
    $('#edit-esi-page-cache-wrapper').show();
    $('#edit-esi-seed-rotation-interval-wrapper').show();
    $('#edit-esi-role-user-direct-injection-wrapper').show();
    $(block_age_fieldset[0]).show();
    $(panel_age_fieldset[0]).show();
    esi_update_cache_visibility();
  }
  // AJAX.
  else if ($('#edit-esi-mode-3:checked').val() !== undefined) {
    $('#edit-esi-ajax-fallback-wrapper').hide();
    esi_update_cdn_ajax_visibility();
    $('#edit-esi-page-cache-wrapper').show();
    $('#edit-esi-seed-rotation-interval-wrapper').show();
    $('#edit-esi-role-user-direct-injection-wrapper').show();
    $(block_age_fieldset[0]).show();
    $(panel_age_fieldset[0]).show();
    esi_update_cache_visibility();
  }
}

$(esi_update_cache_visibility);
$(function(){ $('#edit-esi-page-cache').change(function (){esi_update_cache_visibility();})});
function esi_update_cache_visibility() {
  var page_cache_fieldset = $('#edit-flush').parents('fieldset');
  // Not Disabled and Using the Page cache.
  if ($('#edit-esi-mode-0:checked').val() === undefined && $('#edit-esi-page-cache:checked').val() !== undefined) {
    $(page_cache_fieldset[0]).show();
  }
  else {
    $(page_cache_fieldset[0]).hide();
  }
}

$(esi_update_cdn_ajax_visibility);
$(function(){ $('#edit-esi-ajax-fallback').change(function (){esi_update_cdn_ajax_visibility();})});
function esi_update_cdn_ajax_visibility() {
  // AJAX mode or ESI mode with AJAX fallback checked.
  if ($('#edit-esi-mode-3:checked').val() !== undefined || ($('#edit-esi-mode-1:checked').val() !== undefined && $('#edit-esi-ajax-fallback:checked').val() !== undefined)) {
    $('#edit-esi-cdn-ajax-wrapper').show();
  }
  else {
    $('#edit-esi-cdn-ajax-wrapper').hide();
  }
}

